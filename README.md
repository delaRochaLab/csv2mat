# csv2mat documentation

csv2mat is a Python tool that converts PyBPOD session data in CSV format to .mat files
which can be processed with the Matlab pipeline developed at the lab. The user can 
specify which animals to analyze and a date range and the script will produce
a single file for animal which will contain the information of all the requested sessions.

## How to use it

1. Download the script:

    `git clone https://delaRochaLab@bitbucket.org/delaRochaLab/csv2mat.git`

2. There are two master branches: `master` for Python 3 and `master-python2` for Python 2
(works in the cluster).

3. Open the user_settings.ini file and edit it with your preferences. The configuration
file contains the following variables: 

    + **PATH_CSV** is the folder where you will have the CSV files that need to be
    converted.
    + **PATH_SAVE** is the folder where you want to save the generated .mat files. If it doesn't exist, it will be generated.
    + **RAT_NAMES** is the list of subjects that you want to analyze. The names are separated
    by a dash (-) and have to match the names in the CSV files. For example: 
    *RAT_NAMES = LE33* (for a single animal) or *RAT_NAMES = LE33-LE34-LE35* (for multiple).
    + **DATE_RANGE** will be the range of dates that you want to convert. It accepts 
    multiple options: put a proper range to convert the sessions in between (for example, 
    *DATE_RANGE = 20180801-20180804* to convert sessions from 1st to 4th of August; note the
    format YYYYMMDD and the fact that both dates are separated by a dash); use the *start*
    and *end* keywords (for example, *DATE_RANGE = start-20180804* or *DATE_RANGE = 20180801-end*) or use the *all* keyword (*DATE_RANGE = all*).
    + **EXCLUDE** can be used to set a range of dates that will be excluded. Multiple ranges can be added separated by commas.
    If none have to be excluded, put "null".
    + **POSTOP_FILTER** can be used to filter sessions which match a postop string. You can have multiple filters separated by a comma,
    for example: *POSTOP_FILTER = no_injection, muscimol_ppc*. Be careful, the strings have to match perfectly! Put 'null' to select all.
    + **VARIATION_FILTER** can be used to filter sessions which match a task variation string. Works identically to POSTOP_FILTER.
    + **EXPERIMENTER**, your name, as will be put inside the Matlab file.

4. Make sure that the CSV files are inside the specified folder and run the script:

    `python main.py`

5. You can also run the script with the **--list** option, which will print a list of all the detected sessions
WITHOUT converting them. Use it to perform a quick check before running the program:

    `python main.py --list`
