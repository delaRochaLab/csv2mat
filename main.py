import configparser
import datetime
import logging
import os
import pprint
import re
import traceback
import argparse

import numpy as np
import scipy.io as sio

from time import gmtime, strftime
from SessionParams import SessionParams, FilteredTask

ROOT_PATH = os.getcwd()
CURRENT_TIME = strftime("%Y%m%d_%H%M%S", gmtime())

config = configparser.ConfigParser()
config.read('user_settings.ini')

parser = argparse.ArgumentParser()
parser.add_argument('--list', action="store_true")
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)

def date_regex(path):
    """
    This function accepts a path corresponding to a CSV file with the format:
    ANIMAL_TASK_DATE.csv and returns the DATE.
    """
    return re.search('[0-9]{8}-[0-9]{6}', path).group(0)
    

def compute_file_dict(path, rat_name, exclude=None, date_range=None):
    """
    Looks for the CSV files in the specified path, sorts them by date and returns
    either all of them or those which are between a range of dates specified
    as a tuple. Returns a dictionary with the rat name as key and the list of
    all sessions in a list of strings:
    {"Rat1": [list_of_files], "Rat3": [list_of_files] ... }  
    """

    def correct_date(date: str, date_range: tuple, exclude: tuple) -> bool:
        """
        Checks if the date is inside the specified range and outside the exclusion ranges;
        in that case returns True. Otherwise, returns False.
        """
        # Check general range. Dates come from the file names, which do not include time;
        # to encompass full days we include midnight hours:
        if date_range is not None:
            lower, upper = date_range
            if lower.lower() == 'start':
                case = date <= upper + '-235959'
            elif upper.lower() == 'end':
                case = date >= lower + '-000000'
            else:
                case = lower + '-000000' <= date <= upper + '-235959'
        else:
            case = True
        
        # Check exclusion range. There can be more than one!
        exclude_case = []  # An array of booleans which, for every exclude range, tells if it's ok or not.
        if exclude is not None:
            for dates in exclude:
                lower_excl, upper_excl = dates.split('-')  
                if lower_excl.lower() == 'start':
                    exclude_case.append(date >= upper_excl + '-235959')
                elif upper_excl.lower() == 'end':
                    exclude_case.append(date <= lower_excl + '-000000')
                else:
                    exclude_case.append(lower_excl + '-000000' >= date or date >= upper_excl + '-235959')
        else:
            exclude_case = [True]

        return case and all(exclude_case)

    def path_generator(path: str, pattern: str):
        """
        This method returns a generator object which recursively finds files in "path"
        which match (end with) the "pattern".
        """
        for root, _, file in os.walk(path):
            for f in file:
                if f.endswith(pattern):
                    yield os.path.join(root, f)

    # Initialize the return dictionaries:
    rat_dict = {rat: [] for rat in rat_name}
    # Populate the paths:
    for path in path_generator(path, ".csv"):
        file_name = re.search('LE\w+-\w+', path).group(0) #  Match from LE to .csv
        animal, *_, date = re.split("_", file_name) #  Grab rat name and date. Unused variable is the task.
        if animal in rat_dict:
            if correct_date(date, date_range, exclude):
                rat_dict[animal].append(path)
    # Now we sort the paths by date and populate the dates dict:
    for rat, path_list in rat_dict.items():
        rat_dict[rat] = sorted(path_list, key=date_regex)

    return rat_dict

def compute_session_params(sessions_list: list, postop_filter, variation_filter):  
    
    total_session_params = []
    converted_files = []
    
    for (nn, session) in enumerate(sessions_list):
        session_date = date_regex(session)
        try:
            session_params = SessionParams(session, postop_filter, variation_filter).session_params
            logging.info(f"\tSession {nn+1} ({session_date}) OK.")
        except FilteredTask:
            logging.info(f"\tSession {nn+1} ({session_date}) FILTERED OUT.")
        except Exception as e: #  If the session is bad for some reason, skip it and save the traceback in a file
            logging.warning(f"\tSession {nn+1} ({session_date}) FAILED.")
            if not os.path.exists("logs"):
                os.mkdir("logs")
            with open(f'logs/log_{CURRENT_TIME}.txt', 'a+') as f:
                f.write(f"Log record for file {session_date}: \n")
                f.write(str(e))
                f.write(traceback.format_exc())
                f.write("-"*25 + "\n")
        else: 
            converted_files.append(session)
            total_session_params.append(session_params)
            
    return converted_files, total_session_params

def check_config() -> (str, str):
    
    try:
        rat_names = config['CONFIG']['RAT_NAMES'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the rat names in the user_settings.ini file.")
    
    try:
        date_range = config['CONFIG']['DATE_RANGE'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the date range in the user_settings.ini file.")
    
    try:
        config['USER']['EXPERIMENTER']
    except KeyError:
        logging.exception(f" Error: You should specify the experimenter name in the user_settings.ini file.")
    
    try:
        config['PATHS']['PATH_CSV']
    except KeyError:
        logging.exception(f" Error: You should specify the path to the CSVs in the user_settings.ini file.")
        
    try:
        config['PATHS']['PATH_SAVE']
    except KeyError:
        logging.exception(f" Error: You should specify the path to save folder in the user_settings.ini file.")   
        
    try:
        config['CONFIG']['EXCLUDE'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the EXCLUDE variable in the user_settings.ini file.")
    
    try:
        config['CONFIG']['POSTOP_FILTER'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the POSTOP_FILTER variable in the user_settings.ini file.")

    try:
        config['CONFIG']['VARIATION_FILTER'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the VARIATION_FILTER variable in the user_settings.ini file.")

    return date_range, rat_names

def main():

    # ------------ INITIAL CHECKUP -----------------------
    date_range, rat_names = check_config()
    path_to_csvs = config['PATHS']['PATH_CSV']
    path_to_save = config['PATHS']['PATH_SAVE']
    experimenter = config['USER']['EXPERIMENTER']
    # Exclude can be None, a single date range separated by '-', or multiple date ranges separated by ','.
    exclude = [date.strip() for date in config['CONFIG']['EXCLUDE'].split(',')]
    if exclude[0].lower() == 'null': # The user didn't specify any dates, just 'null'.
        exclude = None    
    if date_range[0].lower() != 'all':
        file_dict = compute_file_dict(path_to_csvs, rat_names, exclude, date_range=date_range)
    else:
        file_dict = compute_file_dict(path_to_csvs, rat_names, exclude)
    # Check filters:
    postop_filter = [postop.strip() for postop in config['CONFIG']['POSTOP_FILTER'].split(',')]
    if postop_filter[0].lower() == 'null':
        postop_filter = None
    variation_filter = [variation.strip() for variation in config['CONFIG']['VARIATION_FILTER'].split(',')]
    if variation_filter[0].lower() == 'null':
        variation_filter = None
    # -------------- MAIN --------------------------
    if args.list:
        printer = pprint.PrettyPrinter(indent=4)
        printer.pprint(file_dict)
    else:    
        for (rat, files) in file_dict.items():
            number_of_sessions = len(files)
            if number_of_sessions:
                logging.info(f' Starting script for rat {rat}. Found {number_of_sessions} sessions inside the specified range.')
                converted_files, session_params = compute_session_params(files, postop_filter, variation_filter)
                if converted_files:
                    logging.info(f' All sessions done. Saving file for rat {rat}. (This may take a while...)')
                    session_names = np.array([re.search('\w+_\w+_[0-9]+-[0-9]+\.csv', session).group(0) for session in converted_files], dtype = object)
                    try:
                        os.chdir(path_to_save)
                    except FileNotFoundError:
                        os.mkdir(path_to_save)
                        os.chdir(path_to_save)
                        
                    if len(converted_files) == 1:
                        file_date = date_regex(converted_files[0])[0:8]
                        sio.savemat(f'{rat}PreProcessData{file_date}.mat', 
                            {'RatName': rat, 
                            'ExperimentSession': session_names, 
                            'BehaviorData': session_params, 
                            'Experimenter': experimenter}, 
                            oned_as = 'column')
                    else:
                        first_date, second_date = date_regex(converted_files[0])[0:8], date_regex(converted_files[-1])[0:8]
                        sio.savemat(f'{rat}PreProcessData{first_date}-{second_date}.mat', 
                            {'RatName': rat, 
                            'ExperimentSession': session_names, 
                            'BehaviorData': session_params, 
                            'Experimenter': experimenter}, 
                            oned_as = 'column')  
                else:
                    logging.warning(f" Files about rat {rat} couldn't be converted. Skipping to next or finishing.") 
            else:
                logging.warning(f" Files about rat {rat} are missing or not inside the specified range. Skipping to next or finishing.")
            print('-'*25)
            
            os.chdir(ROOT_PATH)


if __name__ == "__main__": main()
