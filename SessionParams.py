import logging
import numpy as np
import pandas as pd


# Some custom exceptions:
class UnsupportedTask(Exception):
    pass


class FilteredTask(Exception):
    pass


class SessionParams:

    def __init__(self, path, postop_filter, variation_filter):
        self._path = path
        self._dataframe = None
        self.session_params = None
        self._length = None
        self._tasktype = None
        self._postop_filters = postop_filter
        self._variation_filters = variation_filter
        # Read the CSV and initialize session_params:
        self._read_dataframe(self._path)
        self._parse_dataframe()

    def _read_dataframe(self, path):
        """
        Take the path of a CSV file and read it as a pandas dataframe.
        """
        try:
            dataframe = pd.read_csv(path, skiprows=6, sep=';')
        except FileNotFoundError:
            logging.critical("Error: CSV file not found. Exiting...")
            raise
        else:
            self._dataframe = dataframe

    def _parse_dataframe(self):

        def calculate_length(punish_data, reward_data, invalids=None):
            """
            Length (total trials) is calculated as the sum of invalids, corrects
            and incorrects.
            """
            if self._tasktype == 'p1_STRF_v2.0':
                inner_length = len(self._dataframe.query("TYPE=='VAL' and MSG=='TORC'")['BPOD-FINAL-TIME'])
            else:
                if invalids is None:
                    inner_length = (punish_data.dropna().size
                                    + reward_data.dropna().size)
                else:
                    inner_length = (punish_data.dropna().size
                                    + reward_data.dropna().size
                                    + invalids.dropna().size)
            return inner_length

        def compute_hithistory(punish_data, invalids):
            """
            Computes the HitHistory vector: contains 1 for correct trials,
            0 for incorrect trials and -3 for invalid trials (old BControl
            convention).
            """
            hithistory = []
            for (ii, elem) in enumerate(punish_data):
                if not invalids[ii]:
                    hithistory.append(-3)
                elif elem:
                    hithistory.append(1)
                else:
                    hithistory.append(0)
            return hithistory

        def obtain_task_type():

            accepted_tasks = {'p3', 'p4', 'pilot_s6_01', 'p4_u', 'p1_STRF_v2.0', 'p4_uncorrelated_new',
                              'p4_repalt_silence_new', 'p4_leftright_silence_new', 'p4_repalt_delay',
                              'p4_leftright_delay', 'p4_uncorrelated_silence_new', 'p4_repalt_silence_new_easy'}
            self._tasktype = self._dataframe[self._dataframe.MSG == 'PROTOCOL-NAME']['+INFO'].iloc[0]
            if self._tasktype not in accepted_tasks:
                raise UnsupportedTask(f"{self._tasktype} is not a supported task.")

        # First of all, check if the task is supported:
        obtain_task_type()

        # Pharma variables; PostOp and task variation for now:
        try:
            postop = self._dataframe[self._dataframe.MSG == 'VAR_POSTOP']['+INFO'].iloc[0]
        except IndexError:
            postop = 'no_injection'
        try:
            variation = self._dataframe[self._dataframe.MSG == 'VAR_VARIATION']['+INFO'].iloc[0]
        except IndexError:
            variation = 'no_injection'
        # Check if the task has to be filtered; if so, raise a FilteredTask exception:
        if self._postop_filters is not None:
            if postop not in self._postop_filters:
                raise FilteredTask
        if self._variation_filters is not None:
            if variation not in self._variation_filters:
                raise FilteredTask
        try:
            stage_number = self._dataframe[self._dataframe.MSG == 'STAGE_NUMBER']['+INFO'].iloc[0]
        except IndexError:
            stage_number = -1
        # If we got here, the session is valid; start computing stuff:
        punish_data = self._dataframe.query("TYPE=='STATE' and MSG=='Punish'")['BPOD-FINAL-TIME']
        invalids = self._dataframe.query("TYPE=='STATE' and MSG=='Invalid'")['BPOD-FINAL-TIME']
        reward_data = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-FINAL-TIME']
        if invalids.size:
            #  Now invalids will contain False for invalid trials and True otherwise:
            self._length = calculate_length(punish_data, reward_data, invalids)
            invalids = invalids.apply(lambda x: np.isnan(float(x))).values[:self._length]
        else:
            self._length = calculate_length(punish_data, reward_data)
            invalids = [True] * self._length
            # logging.warning("This session didn't take invalid trials into account.")
        assert self._length > 0, "Session results not found; report can't be generated. Exiting..."
        #  Now punish_data will contain True if the answer was correct, False if incorrect or invalid:
        punish_data = punish_data.apply(lambda x: np.isnan(float(x))).values[:self._length]
        #  reward_side contains a 1 if the correct answer was the (right)ight side, 0 otherwise:
        try:
            reward_side = self._dataframe[self._dataframe.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1][1:-1].split(',')
        except IndexError:  # Compatibility with old files
            # logging.warning("REWARD_SIDE vector not found. Trying old VECTOR_CHOICE...")
            try:
                reward_side = self._dataframe[self._dataframe.MSG == 'VECTOR_CHOICE']['+INFO'].iloc[-1][1:-1].split(',')
            except IndexError:
                logging.critical("Neither REWARD_SIDE nor VECTOR_CHOICE found. Exiting...")
                raise
        self.reward_side = reward_side  # Complete rewards side for the prob repeat function below
        reward_side = np.transpose(np.array([int(x) for x in reward_side[:self._length]], dtype=np.object))
        coherences = self._dataframe[self._dataframe['MSG'] == 'coherence01']['+INFO'].values[:self._length]
        coherences = coherences.astype(float)
        #  List of possible, unique coherences:
        coherence_vector = sorted(list(set(coherences)))
        #  Coherences for every trial, as indices of coherence_vector. Remember
        #  that Matlab indices start at 1:
        coherence_list = [coherence_vector.index(elem) + 1 for elem in
                          coherences]
        coherence_vector = np.array(coherence_vector)

        # includes silence_trial data
        alltrials = self._dataframe.query("TYPE=='TRIAL' and MSG=='New trial'")['PC-TIME']
        siltrials = self._dataframe.query("TYPE=='VAL' and MSG=='silence_trial'")['PC-TIME']
        silences = np.array([])
        for trial_time in alltrials:
            trial_sum = 0
            for silence_time in siltrials:
                if trial_time[11:19] in silence_time[11:19]:
                    trial_sum += 1
            if trial_sum > 0:
                silences = np.append(silences, [1])
            else:
                silences = np.append(silences, [0])

        # includes delay data
        delaytrials = self._dataframe[self._dataframe['MSG'] == 'delay_trial']['+INFO'].values[:self._length]
        delays = delaytrials.astype(float)
        # list(delaytrials)

        #  1 for left, 2 for right:
        rewardside_list = [1 if elem == 0 else 2 for elem in reward_side]
        hithistory = compute_hithistory(punish_data, invalids)
        # left_envelopes contains a list of strings:
        left_envelopes = self._dataframe.query("TYPE=='VAL' and MSG=='left_envelope'")['+INFO'].values[:self._length]
        right_envelopes = self._dataframe.query("TYPE=='VAL' and MSG=='right_envelope'")['+INFO'].values[:self._length]

        left = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in left_envelopes]
        right = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in right_envelopes]

        # DETECTING SOUND FAIL
        # adding a coherence index
        self._dataframe.loc[(self._dataframe.TYPE == 'VAL') &
                            (self._dataframe['MSG'] == 'coherence01'), 'coherence_index'] = \
            np.arange(0, len(self._dataframe.loc[(self._dataframe.TYPE == 'VAL') &
                                                 (self._dataframe['MSG'] == 'coherence01')]))
        self._dataframe['coherence_index'].fillna(method='ffill', inplace=True)

        # sound_fail = boolean list indicating when play is not present
        sr_play = self._dataframe.loc[(self._dataframe.MSG.str.startswith('SoundR: Play.')) &
                                      (self._dataframe.TYPE == 'stdout'), 'coherence_index'].values
        all_trials = np.arange(self._length)
        sound_fail = np.isin(all_trials, sr_play, assume_unique=True, invert=True)

        # making left and right zero when sound_fail
        left = [np.repeat(0, len(left[i])) if sound_fail[i] else left[i] for i in range(len(left))]
        right = [np.repeat(0, len(right[i])) if sound_fail[i] else right[i] for i in range(len(right))]

        # giving coherence list an absurd value (-100) when sound_fail
        for (i, item) in enumerate(coherence_list):
            if sound_fail[i]:
                coherence_list[i] = -100
        # in session_params we add a column NoSound

        # REAL SOUNDONSET AND SOUNDOFFSET
        # we get the values of onset and offset from messages 60, 61, 62, 63
        test = self._dataframe[self._dataframe['MSG'].isin(['60', '61', '62', '63'])]
        test = test[test.TYPE != 'STATE']

        testa = test[test.MSG.isin(['60', '62'])].drop_duplicates(subset=['coherence_index'], keep='first')
        testa.index = testa.coherence_index
        testa = testa.reindex(all_trials)
        sound_onset = testa['BPOD-INITIAL-TIME'].to_numpy()

        testb = test[test.MSG.isin(['61', '63'])].drop_duplicates(subset=['coherence_index'], keep='last')
        testb.index = testb.coherence_index
        testb = testb.reindex(all_trials)
        sound_offset = testb['BPOD-INITIAL-TIME'].to_numpy()

        # STIMULUS
        envelope_diff = np.transpose(np.array([elem + right[mm] for (mm, elem) in enumerate(left)])[np.newaxis])
        stimulus_ind = np.transpose(np.array(list(range(1, len(left_envelopes) + 1)))[np.newaxis])
        stimulus_right = np.transpose(right)
        stimulus_left = np.transpose(left)

        if self._tasktype == 'p3':
            env_prob_repeat = [0.5] * self._length  # For p3, prob_repeat is always p=0.5
        else:
            prob_repeat = self._dataframe[self._dataframe['MSG'] == 'prob_repeat']['+INFO'].values[:self._length]
            if not prob_repeat.size:  # The session is old, and prob_repeat is missing
                # So we compute it:
                prob_repeat = self._compute_prob_repeat()[:self._length] / 100

            # prob_repeat = prob_repeat.astype(float)
            if type(prob_repeat[0]) is not str:
                prob_repeat = prob_repeat.astype(float)

            env_prob_repeat = list(prob_repeat)

        self.session_params = {'CoherenceVec': coherence_vector,
                               'RewardSideList': rewardside_list,
                               'HitHistoryList': hithistory,
                               'CoherenceList': coherence_list,
                               'SilenceList': silences,  # trials on purpose with no sound
                               'DelayList': delays,
                               'StimulusList': stimulus_ind,
                               'EnviroProbRepeat': env_prob_repeat,
                               'StageNumber': stage_number,
                               'StimulusBlock': envelope_diff,
                               'StimulusR': stimulus_right,
                               'StimulusL': stimulus_left,
                               'TaskVariation': variation,
                               'PharmaPostOp': postop,
                               'NoSound': sound_fail,  # trials when the sound failed to play
                               'SoundOnset': sound_onset,  # first message back sound is playing
                               'SoundOffset': sound_offset}  # last message back sound is playing

        if self._tasktype != "p1_STRF_v2.0":
            parsed_events = {'ParsedEvents': np.transpose(self._parsed_events()[np.newaxis])}
            self.session_params.update(parsed_events)

        if self._tasktype == 'p1_STRF_v2.0':
            sound_names = {'SoundNames': self._dataframe.query(
                "TYPE=='VAL' and MSG=='TORC'")['+INFO'].values[:self._length]}
            self.session_params.update(sound_names)

    def _compute_prob_repeat(self):
        """
        This function computes the probability of repeat of the blocks that form the session.
        It is used in those sessions which were p4 but which didn't have yet the data in the CSV
        (sessions before the 10th of august, 2018).
        """

        def getrep(trial_list, blen):
            nblocks = int(round(len(trial_list) / blen, 0))
            transitions = np.arange(nblocks, step=1) * blen
            segmentrepprobs = []
            for item in transitions:
                segment = trial_list[item:item + blen]
                rep = 0
                for jj, elem in enumerate(segment, 1):
                    if elem == segment[jj - 1]:
                        rep += 1
                segmentrepprobs = segmentrepprobs + [rep]
            thereps = np.array(segmentrepprobs) * 100 / (blen - 1)
            first, second = np.arange(0, nblocks, step=2), np.arange(1, nblocks, step=2)

            probreporder = [80, 20] if thereps[first].mean() > thereps[second].mean() else [20, 80]

            return np.repeat(probreporder * int(nblocks / 2), blen)

        bsize = int(
            self._dataframe.loc[(self._dataframe.TYPE == 'VAL') & (self._dataframe.MSG == 'VAR_BLEN'), '+INFO'].values[
                0])

        return getrep(self.reward_side, bsize)

    @staticmethod
    def _cols(*argv):
        """
        Helper function. This method packs data into a vertical shape so that
        Matlab recognizes it as a column vector.
        """
        if len(argv) == 2:
            columns = np.column_stack((argv[0], argv[1]))
        elif len(argv) == 1:
            columns = np.column_stack((argv[0], argv[0]))
        else:
            raise TypeError("This function accepts at most 2 arguments.")
        return columns

    def _timestamp_vectors(self, vec):
        """
        Helper function. This method takes a numpy array, returns a number of values
        equal to the trial length (self._length) and casts the values to float.
        """
        column = vec.values[:self._length].astype(float)
        return column

    def _parsed_events(self):
        """
        This method takes the session dataframe and parses the timestamps, converting from
        PyBPOD state names to old BControl names. It returns a numpy array that will be
        later saved into the .mat file as a 'parsedEvents' cell.
        """

        trials_start = self._timestamp_vectors(
            self._dataframe.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-INITIAL-TIME'])
        trials_end = self._dataframe.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-FINAL-TIME'].values[
                     :self._length].astype(float)
        trials_nan = np.array([np.nan] * self._length)

        play_target_start = self._dataframe.query("TYPE=='STATE' and MSG=='StartSound'")['BPOD-INITIAL-TIME'].values[
                            :self._length].astype(float) + trials_start
        try:
            play_target_end = self._dataframe.query("TYPE=='STATE' and MSG=='KeepSoundOn'")['BPOD-FINAL-TIME'].values[
                              :self._length].astype(float) + trials_start
        except ValueError:
            play_target_end = self._dataframe.query("TYPE=='STATE' and MSG=='StartSound'")['BPOD-FINAL-TIME'].values[
                              :self._length].astype(float) + trials_start
        play_target_start = np.array([elem if not np.isnan(elem) else [] for elem in list(play_target_start)],
                                     dtype=np.object)
        play_target_end = np.array([elem if not np.isnan(elem) else [] for elem in list(play_target_end)],
                                   dtype=np.object)

        wait_apoke_start = self._dataframe.query("TYPE=='STATE' and MSG=='WaitResponse'")['BPOD-INITIAL-TIME'].values[
                           :self._length].astype(float) + trials_start
        wait_apoke_end = self._dataframe.query("TYPE=='STATE' and MSG=='WaitResponse'")['BPOD-FINAL-TIME'].values[
                         :self._length].astype(float) + trials_start
        wait_apoke_start = np.array([elem if not np.isnan(elem) else [] for elem in list(wait_apoke_start)],
                                    dtype=np.object)
        wait_apoke_end = np.array([elem if not np.isnan(elem) else [] for elem in list(wait_apoke_end)],
                                  dtype=np.object)

        reward_start = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-INITIAL-TIME'].values[
                       :self._length].astype(float) + trials_start
        reward_end = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-FINAL-TIME'].values[
                     :self._length].astype(float) + trials_start
        reward_start = np.array([elem if not np.isnan(elem) else [] for elem in list(reward_start)], dtype=np.object)
        reward_end = np.array([elem if not np.isnan(elem) else [] for elem in list(reward_end)], dtype=np.object)

        intertrial_interval_start = trials_end
        intertrial_interval_end = np.append(trials_start[1:], trials_end[-1])

        new_trial_indexes = self._dataframe.query("TYPE=='TRIAL' and MSG=='New trial'").index[:self._length]
        parsed_events_cell = np.zeros((self._length,), dtype=np.object)
        for jj in range(self._length):
            states = {'starting_state': 'state_0', 'ending_state': 'final_state'}

            if jj != self._length - 1:
                index_1, index_2 = new_trial_indexes[jj], new_trial_indexes[jj + 1]
                trial_band_events = self._dataframe.query("TYPE=='EVENT' and index > @index_1 and index < @index_2")
                trial_band_states = self._dataframe.query("TYPE=='STATE' and index > @index_1 and index < @index_2")
            else:
                index_1 = new_trial_indexes[jj]
                trial_band_events = self._dataframe.query("TYPE=='EVENT' and index > @index_1")
                trial_band_states = self._dataframe.query("TYPE=='STATE' and index > @index_1")
            wait_cpoke_start = trial_band_states.query(
                "MSG=='WaitCPoke'")['BPOD-INITIAL-TIME'].values.astype(float) + trials_start[jj]
            wait_cpoke_end = trial_band_states.query(
                "MSG=='WaitCPoke'")['BPOD-FINAL-TIME'].values.astype(float) + trials_start[jj]

            pre_stim_delay_start = trial_band_states.query(
                "MSG=='Fixation'")['BPOD-INITIAL-TIME'].values.astype(float) + trials_start[jj]
            pre_stim_delay_end = trial_band_states.query(
                "MSG=='Fixation'")['BPOD-FINAL-TIME'].values.astype(float) + trials_start[jj]

            states['state_0'] = self._cols(trials_nan[jj], trials_end[jj])
            states['wait_for_apoke'] = self._cols(wait_apoke_start[jj], wait_apoke_end[jj])
            states['reward'] = self._cols(reward_start[jj], reward_end[jj])
            states['check_next_trial_ready'] = self._cols(intertrial_interval_end[jj])
            states['play_target'] = self._cols(play_target_start[jj], play_target_end[jj])
            states['intertrial_interval'] = self._cols(intertrial_interval_start[jj], intertrial_interval_end[jj])
            states['final_state'] = states['check_next_trial_ready']
            states['wait_for_cpoke'] = self._cols(wait_cpoke_start, wait_cpoke_end)
            states['send_trial_info'] = self._cols(wait_cpoke_start[0])
            states['pre_stim_delay'] = self._cols(pre_stim_delay_start, pre_stim_delay_end)
            a = []
            if states['pre_stim_delay'].size > 2:
                for mm in range(0, (states['pre_stim_delay'].size - 2) // 2):
                    a.append(states['pre_stim_delay'][mm, 1])
            b = np.array(a)
            states['early_withdrawal_pre'] = self._cols(b)

            found_l_in = found_c_in = found_r_in = found_l_out = found_c_out = found_r_out = False
            c_start = []
            l_start = []
            r_start = []
            c_end = []
            l_end = []
            r_end = []
            for (_, row) in trial_band_events.iterrows():
                if row['MSG'] == '68':  # Port1In
                    found_l_in = True
                    if found_l_out:
                        l_start.append(np.nan)
                        found_l_out = False
                    l_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '70':  # Port2In
                    found_c_in = True
                    if found_c_out:
                        c_start.append(np.nan)
                        found_c_out = False
                    c_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '72':  # Port3In
                    found_r_in = True
                    if found_r_out:
                        r_start.append(np.nan)
                        found_r_out = False
                    r_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '69':  # Port1Out
                    if not found_l_in:
                        found_l_out = True
                    else:
                        found_l_in = False
                    l_end.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '71':  # Port2Out
                    if not found_c_in:
                        found_c_out = True
                    else:
                        found_c_in = False
                    c_end.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '73':  # Port3Out
                    if not found_r_in:
                        found_r_out = True
                    else:
                        found_r_in = False
                    r_end.append(row['BPOD-INITIAL-TIME'])
                else:
                    pass

            if found_l_in:
                l_end.append(np.nan)
            if found_c_in:
                c_end.append(np.nan)
            if found_r_in:
                r_end.append(np.nan)

            if found_l_out:
                l_start.append(np.nan)
            if found_c_out:
                c_start.append(np.nan)
            if found_r_out:
                r_start.append(np.nan)

            c = np.column_stack((c_start + trials_start[jj], c_end + trials_start[jj]))
            left = np.column_stack((l_start + trials_start[jj], l_end + trials_start[jj]))
            right = np.column_stack((r_start + trials_start[jj], r_end + trials_start[jj]))

            pokes = {'L': left, 'R': right, 'c': c}
            upper_struct = {'states': states, 'pokes': pokes}
            parsed_events_cell[jj] = upper_struct

        return parsed_events_cell
